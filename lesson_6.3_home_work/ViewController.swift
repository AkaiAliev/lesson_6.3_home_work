//
//  ViewController.swift
//  lesson_6.3_home_work
//
//  Created by Akai on 16/3/23.
//

import UIKit

//class ViewController: UIViewController {
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        // Do any additional setup after loading the view.
//    }
//
//
//}

// MARK: - Factory

enum CarsType {
    case sedan
    case crossover
    case offroad
}

protocol Cars {
    var name: String { get }
    var model: String { get }
    var type: CarsType { get }
    var producerСountry: String { get }
    
    func drivingStyle()
}

class Sedan: Cars {
    var name: String = "Toyota"
    var model: String = "Camry"
    var type: CarsType = .sedan
    var producerСountry: String = "Japan"
    
    func drivingStyle() {
        print("The \(name) \(model) is made in \(producerСountry) by \(name) company. This car is for city driving.")
    }
}

class Crossover: Cars {
    var name: String = "Kia"
    var model: String = "Sportage"
    var type: CarsType = .crossover
    var producerСountry: String = "South Korea"
    
    func drivingStyle() {
        print("The \(name) \(model) is made in \(producerСountry) by \(name) company. This car is for city driving and off-roading.")
    }
}

class Offroad: Cars {
    var name: String = "Nissan"
    var model: String = "Patrol"
    var type: CarsType = .offroad
    var producerСountry: String = "Japan"
    
    func drivingStyle() {
        print("The \(name) \(model) is made in \(producerСountry) by \(name) company. This car is for driving on the mountains and off-roading.")
    }
}

class CarsFactory {
    static let shared = CarsFactory()
    private init() { }
    
    func createCars(type: CarsType) -> Cars {
        switch type {
            
        case .sedan:
            return Sedan()
        case .crossover:
            return Crossover()
        case .offroad:
            return Offroad()
        }
    }
}

let sedan = CarsFactory.shared.createCars(type: .sedan)
let crossover = CarsFactory.shared.createCars(type: .crossover)
let offroad = CarsFactory.shared.createCars(type: .offroad)

var allCars: [Cars] = [sedan, crossover, offroad]

for car in allCars {
    car.drivingStyle()
}


enum ClothesType {
    case outerwear
    case shoes
}

protocol Clothes {
    var title: String { get }
    var type: ClothesType { get }
    var color: String { get }
    
    func putOn()
}

class Jacket: Clothes {
    var title: String = "Pull&Bear"
    var type: ClothesType = .outerwear
    var color: String = "Dark Green"
    
    func putOn() {
        print("Wearing \(color) \(title) jacket")
    }
}

class Sneakers: Clothes {
    var title: String = "Nike"
    var type: ClothesType = .shoes
    var color: String = "Black"
    func putOn() {
        print("Wearing \(color) \(title) sneakers")
    }
}

class ClothesFactory {
    static let shared = ClothesFactory()
    private init() { }
    
    func createClothes(type: ClothesType) -> Clothes {
        switch type {
            
        case .outerwear:
            return Jacket()
        case .shoes:
            return Sneakers()
        }
    }
}

let jacket = ClothesFactory.shared.createClothes(type: .outerwear)
let sneakers = ClothesFactory.shared.createClothes(type: .shoes)
var allClothes: [Clothes] = [jacket, sneakers]

for clothes in allClothes {
    clothes.putOn()
}


// MARK: - Builder

class Car {
    var wheels: Int?
    var body: Int?
    var engine: Int?
    var doors: Int?
    var windows: Int?
    var seats: Int?
}

class CarBuilder {
    private var car: Car
    
    init() {
        self.car = Car()
    }
    
    func setWheels(_ count: Int) -> CarBuilder {
        self.car.wheels = count
        return self
    }
    
    func setBody(_ count: Int) -> CarBuilder {
        self.car.body = count
        return self
    }
    
    func setEngine(_ count: Int) -> CarBuilder {
        self.car.engine = count
        return self
    }
    
    func setDoors(_ count: Int) -> CarBuilder {
        self.car.doors = count
        return self
    }
    
    func setWindows(_ count: Int) -> CarBuilder {
        self.car.windows = count
        return self
    }
    
    func setSeats(_ count: Int) -> CarBuilder {
        self.car.seats = count
        return self
    }
    
    func createCar() -> Car {
        return self.car
    }
}

let car = CarBuilder()
    .setWheels(4)
    .setBody(1)
    .setEngine(1)
    .setDoors(6)
    .setWindows(6)
    .setSeats(4)
    .createCar()

print("Для строительства одного автомобиля нужно:\nколесо - \(car.wheels) шт\nкузов - \(car.body) шт\nдвигатель - \(car.engine) шт\nдверь - \(car.doors) шт\nокно - \(car.windows) шт\nсиденье - \(car.seats) шт")


class CrabsBurger {
    var firstIngredient: String?
    var secondIngredient: String?
    var thirdIngredient: String?
    var fourthIngredient: String?
    var fifthIngredient: String?
    var sixthIngredient: String?
    var seventhIngredient: String?
    var eighthIngredient: String?
    var ninthIngredient: String?
    var tenthIngredient: String?
}

class CrabsBurgerBuilder {
    private var crabsBurger: CrabsBurger
    
    init() {
        self.crabsBurger = CrabsBurger()
    }
    
    func setFirstIngredient(_ firstIngredient: String) -> CrabsBurgerBuilder {
        self.crabsBurger.firstIngredient = firstIngredient
        return self
    }
    
    func setSecondIngredient(_ secondIngredient: String) -> CrabsBurgerBuilder {
        self.crabsBurger.secondIngredient = secondIngredient
        return self
    }
    
    func setThirdIngredient(_ thirdIngredient: String) -> CrabsBurgerBuilder {
        self.crabsBurger.thirdIngredient = thirdIngredient
        return self
    }
    
    func setFourthIngredient(_ fourthIngredient: String) -> CrabsBurgerBuilder {
        self.crabsBurger.fourthIngredient = fourthIngredient
        return self
    }
    
    func setFifthIngredient(_ fifthIngredient: String) -> CrabsBurgerBuilder {
        self.crabsBurger.fifthIngredient = fifthIngredient
        return self
    }
    
    func setSixthIngredient(_ sixthIngredient: String) -> CrabsBurgerBuilder {
        self.crabsBurger.sixthIngredient = sixthIngredient
        return self
    }
    
    func setSeventhIngredient(_ seventhIngredient: String) -> CrabsBurgerBuilder {
        self.crabsBurger.seventhIngredient = seventhIngredient
        return self
    }
    
    func setEighthIngredient(_ eighthIngredient: String) -> CrabsBurgerBuilder {
        self.crabsBurger.eighthIngredient = eighthIngredient
        return self
    }
    
    func setNinthIngredient(_ ninthIngredient: String) -> CrabsBurgerBuilder {
        self.crabsBurger.ninthIngredient = ninthIngredient
        return self
    }
    
    func setTenthIngredient(_ tenthIngredient: String) -> CrabsBurgerBuilder {
        self.crabsBurger.tenthIngredient = tenthIngredient
        return self
    }
    
    
    func createCrabsBurger() -> CrabsBurger {
        return self.crabsBurger
    }
}

let crabsBurger = CrabsBurgerBuilder()
    .setFirstIngredient("Булка")
    .setSecondIngredient("Котлета")
    .setThirdIngredient("Салат")
    .setFourthIngredient("Сыр")
    .setFifthIngredient("Лук")
    .setSixthIngredient("Томат")
    .setSeventhIngredient("Кетчуп")
    .setEighthIngredient("Горчица")
    .setNinthIngredient("Пикули")
    .setTenthIngredient("Булка")
    .createCrabsBurger()

print("Рецепт приготовления крабсбургера:\n\(crabsBurger.firstIngredient)\n\(crabsBurger.secondIngredient)\n\(crabsBurger.thirdIngredient)\n\(crabsBurger.fourthIngredient)\n\(crabsBurger.fifthIngredient)\n\(crabsBurger.sixthIngredient)\n\(crabsBurger.seventhIngredient)\n\(crabsBurger.eighthIngredient)\n\(crabsBurger.ninthIngredient)\n\(crabsBurger.tenthIngredient)\n")


// MARK: - Facade

//Скидка
class Discount {
    //    функция принимает в себя количество пицц и возвращает сумму скидки
    func getDiscount(countOfPizza: Int) -> Double {
        switch countOfPizza {
            //количество пицц
        case 1:
            //сумма скидки
            return 0
        case 2...5:
            return 10
        default:
            return 20
        }
        
    }
}

//рассчет цены на заказ согласно количеству пицц
class Order {
    func getPrice(countOfPizza: Int) -> Double {
        //средняя цена на пиццу
        return Double(countOfPizza * 450)
    }
}

//определяет время доставки
class Delivery {
    func getTimeDelivery(address: String) -> Int {
        if address == "Bishkek" {
            return 60
        } else {
            return 120
        }
    }
}

class Pizzeria {
    let discout: Discount
    let order: Order
    let delivery: Delivery
    
    init(discout: Discount, order: Order, delivery: Delivery) {
        self.discout = discout
        self.order = order
        self.delivery = delivery
    }
    
    func getInfo(countOfPizza: Int, address: String) -> (Double, Int) {
        //обычная цена
        let regularPrice = order.getPrice(countOfPizza: countOfPizza)
        let discountPrice = discout.getDiscount(countOfPizza: countOfPizza)
        //сумма оплаты
        let price = regularPrice - regularPrice * discountPrice/100
        //время доставки
        let timeDelivery = delivery.getTimeDelivery(address: address)
        return (price, timeDelivery)
    }
}

let callCenter = Pizzeria(discout: Discount(), order: Order(), delivery: Delivery())
let firstCustomer = callCenter.getInfo(countOfPizza: 5, address: "Bishkek")
print(firstCustomer)
let secondCustomer = callCenter.getInfo(countOfPizza: 2, address: "Kant")
print(secondCustomer)


class MovieGeek {
    
    func findMovie() {
        print("Сеансы на сегодня:\nТитаник\nАватар\nКинг-Конг\nРусалочка")
    }
    
    func filmSelectionRequest() {
        print("Выберите фильм!")
    }
    
    func filmSelection(movie: String) {
        print("Вы выбрали фильм \(movie)!")
    }
}

class TicketPurchase {
    
    func methodOfPayment() {
        print("Выберите способ оплаты:\n- Оплата платежной картой\n- Оплата наличными на кассе")
        
    }
    
    func choiceOfPaymentMethod() {
        print("Оплата платежной картой")
    }
    func responce() {
        print("Вы выбрали способ оплаты платежной картой!")
    }
    func ticketsArePurchased() {
        print("Билеты куплены! Приятного просмотра!")
    }
    
}

class MovieNight {
    func goToThe(movie:String) {
        let movieGeek = MovieGeek()
        let ticketPurchase = TicketPurchase()
        
        movieGeek.findMovie()
        movieGeek.filmSelectionRequest()
        movieGeek.filmSelection(movie: "Титаник")
        ticketPurchase.methodOfPayment()
        ticketPurchase.choiceOfPaymentMethod()
        ticketPurchase.responce()
        ticketPurchase.ticketsArePurchased()
    }
}

let facade = MovieNight()
facade.goToThe(movie: "Титаник")

//
